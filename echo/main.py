# from datetime import datetime
from subprocess import Popen
from subprocess import PIPE

from telegram import Bot
from telegram import Update
# from telegram import InlineKeyboardButton
# from telegram import InlineKeyboardMarkup
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
# from telegram.ext import CallbackQueryHandler

from echo.config import load_config


# CALLBACK_BUTTON1_LEFT = "callback_button1_left"
# CALLBACK_BUTTON2_RIGHT = "callback_button2_right"
# CALLBACK_BUTTON3_MORE = "callback_button3_more"
# CALLBACK_BUTTON4_BACK = "callback_button4_back"
# CALLBACK_BUTTON5_TIME = "callback_button5_time"
# CALLBACK_BUTTON6_PRICE = "callback_button6_price"
# CALLBACK_BUTTON7_PRICE = "callback_button7_price"
# CALLBACK_BUTTON8_PRICE = "callback_button8_price"
# CALLBACK_BUTTON_HIDE_KEYBOARD = "callback_button9_hide"


# TITLES = {
#     CALLBACK_BUTTON1_LEFT: "Новое сообщение ⚡️",
#     CALLBACK_BUTTON2_RIGHT: "Отредактировать ✏️",
#     CALLBACK_BUTTON3_MORE: "Ещё ➡️",
#     CALLBACK_BUTTON4_BACK: "Назад ⬅️",
#     CALLBACK_BUTTON5_TIME: "Время ⏰",
#     CALLBACK_BUTTON6_PRICE: "BTC 💰",
#     CALLBACK_BUTTON7_PRICE: "LTC 💰",
#     CALLBACK_BUTTON8_PRICE: "ETH 💰",
#     CALLBACK_BUTTON_HIDE_KEYBOARD: "Спрять клавиатуру",
# }

# def get_base_inline_keyboard():
#     keyboard =[
#         [
#             InlineKeyboardButton(TITLES[CALLBACK_BUTTON1_LEFT], callvack_data=CALLBACK_BUTTON1_LEFT),
#             InlineKeyboardButton(TITLES[CALLBACK_BUTTON2_RIGHT], callvack_data=CALLBACK_BUTTON2_RIGHT),
#         ],
#         [
#             InlineKeyboardButton(TITLES[CALLBACK_BUTTON3_MORE], callvack_data=CALLBACK_BUTTON3_MORE),
#         ],
#     ]
#     return InlineKeyboardMarkup(keyboard)

# def keyboard_callback_handler(bot:Bot, update: Update, chat_data=None, **kwards):
#
#     query = update.callback_query
#     data = query.data
#     now = datetime.datetime.now()
#
#     # Обратите внимание: используется `effective_message`
#     chat_id = update.effective_message.chat_id
#     current_text = update.effective_message.text

    # if data == CALLBACK_BUTTON1_LEFT:
    #     # "Удалим" клавиатуру у прошлого сообщения
    #     # (на самом деле отредактируем его так, что текст останется тот же, а клавиатура пропадёт)
    #     query.edit_message_text(
    #         text=current_text,
    #         # parse_mode=ParseMode.MARKDOWN,
    #     )
    #     # Отправим новое сообщение при нажатии на кнопку
    #     context.bot.send_message(
    #         chat_id=chat_id,
    #         text="Новое сообщение\n\ncallback_query.data={}".format(data),
    #         reply_markup=get_base_inline_keyboard(),
    #     )
    #     elif data == CALLBACK_BUTTON2_RIGHT:
    #     # Отредактируем текст сообщения, но оставим клавиатуру
    #     query.edit_message_text(
    #         text="Успешно отредактировано в {}".format(now),
    #         reply_markup=get_base_inline_keyboard(),
    #     )
    # elif data == CALLBACK_BUTTON3_MORE:
    #     # Показать следующий экран клавиатуры
    #     # (оставить тот же текст, но указать другой массив кнопок)
    #     query.edit_message_text(
    #         text=current_text,
    #         reply_markup=get_keyboard2(),
    #     )
    # elif data == CALLBACK_BUTTON4_BACK:
    #     # Показать предыдущий экран клавиатуры
    #     # (оставить тот же текст, но указать другой массив кнопок)
    #     query.edit_message_text(
    #         text=current_text,
    #         reply_markup=get_base_inline_keyboard(),
    #     )
    # elif data == CALLBACK_BUTTON5_TIME:
    #     # Покажем новый текст и оставим ту же клавиатуру
    #     text = "*Точное время*\n\n{}".format(now)
    #     query.edit_message_text(
    #         text=text,
    #         parse_mode=ParseMode.MARKDOWN,
    #         reply_markup=get_keyboard2(),
    #     )
    # elif data in (CALLBACK_BUTTON6_PRICE, CALLBACK_BUTTON7_PRICE, CALLBACK_BUTTON8_PRICE):
    #     pair = {
    #         CALLBACK_BUTTON6_PRICE: "USD-BTC",
    #         CALLBACK_BUTTON7_PRICE: "USD-LTC",
    #         CALLBACK_BUTTON8_PRICE: "USD-ETH",
    #     }[data]
    #
    #     try:
    #         current_price = client.get_last_price(pair=pair)
    #         text = "*Курс валюты:*\n\n*{}* = {}$".format(pair, current_price)
    #     except BittrexError:
    #         text = "Произошла ошибка :(\n\nПопробуйте ещё раз"
    #     query.edit_message_text(
    #         text=text,
    #         parse_mode=ParseMode.MARKDOWN,
    #         reply_markup=get_keyboard2(),
    #     )

def do_start(bot:Bot, update: Update):
    bot.send_message(
        chat_id=update.message.chat_id,
        text="Привет! Отправь мне что-нибудь",
    )



def do_help(bot:Bot, update: Update):
    bot.send_message(
        chat_id=update.message.chat_id,
        text="Це навчальний бот\n\n"
             " Список доступних команд э в меню\n\n"
             " Також я выдповыдаю на любе повыдомлення ",
        # reply_markup=get_base_inline_keyboard(),
    )

def do_time(bot:Bot, update: Update):
    process = Popen("date /t", shell=True, stdout=PIPE)
    text, error = process.communicate()
    if error:
        text = "Виникла помилка, час невідомий"
    else:
        text = text.decode("utf-8")
    bot.send_message(
        chat_id=update.message.chat_id,
        text=text,
        # reply_markup=get_base_inline_keyboard(),
                     )

def do_echo(bot:Bot, update: Update):
    text = update.message.text
    bot.send_message(
        chat_id=update.message.chat_id,
        text=text,
        # reply_markup=get_base_inline_keyboard(),
    )

def main():
    config = load_config

    bot = Bot(
        token=config.TG_TOKEN,
    )
    updater = Updater(
        bot=bot,
    )

    start_handler = CommandHandler("start", do_start)
    help_handler = CommandHandler("help", do_help)
    time_handler = CommandHandler("time", do_time)
    message_handler = MessageHandler(Filters.text, do_echo)

    updater.dispatcher.add_handler(start_handler)
    updater.dispatcher.add_handler(help_handler)
    updater.dispatcher.add_handler(time_handler)
    updater.dispatcher.add_handler(message_handler)

    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
